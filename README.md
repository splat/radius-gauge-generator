# Radius Gauge Generator

### OpenSCAD File.

radius-gauge.scad takes one variable for the radius you want to make a gauge for.

---

### Python Script.

make-gauges.py has 4 variables to modify.

- stl_folder - the folder name to output generated stl files to.
- start - what is the smallest gauge to generate.
- step - this is the amount it increments each time it goes through the loop
- stop - this is the largest gauge to generate.