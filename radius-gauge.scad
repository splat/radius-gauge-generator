module gauge(radius)
{
    straight_edge = max(10, radius / 2);
    base_size = 28;
    diameter = radius * 2;
    width = max(base_size, straight_edge + diameter);
    height = max(base_size, straight_edge + diameter);
    center_x = width / 2;
    centre_y = height / 2;
    thickness = 2;
    
    text_size = max(4, 2+(radius/10));
    text_position_x = 8;
    text_position_y = height - (text_size * 2);
    hole_radius = 2;
    hole_position_x = hole_radius * 2;
    hole_position_y = height - hole_radius * 2;
    text = str(radius, "mm");
    difference()
    {
        union()
        {
            translate([width-radius, height-radius, 0])
            {
                intersection()
                {
                    cylinder(h = thickness, r = radius, $fn=100);
                    cube([diameter, diameter, 5], center = false);
                }
            }
            difference()
            {
                difference() 
                {
                    cube([width, height, thickness]);
                    translate([width, straight_edge, -1])
                        cylinder(h = 5, r = radius, $fn=100);
                    translate([hole_position_x, hole_position_y, -1])
                        cylinder(h = 5, r = hole_radius, $fn=100);
                    translate([width-radius, -1, -1])
                        cube([radius+1, straight_edge+1, thickness * 2], center = false);
                    translate([-radius / 2, straight_edge, -1])
                        cylinder(h = 5, r = radius, $fn=100);
                    translate([-radius / 2, -1, -1])
                        cube([radius, straight_edge+1, 5], center = false);
                }
                translate([width-radius, height-radius, -1])
                    cube([diameter, diameter, 5], center = false);
            }
            
        } 
        translate([text_position_x, text_position_y, thickness-1])    
            color("Blue", 1.0)
                linear_extrude(height = 2)
                    text(text, size = text_size, halign="left", valign="top", $fn=100);
    }
        

}

radius = 5.0;
gauge(radius = radius);