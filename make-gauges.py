import subprocess
from pathlib import Path

# Folder for generated stl files. Needs to be it's own folder.
stl_folder = "stls"

# Starting size of radius gauges.
start = 1.0

# Step size for loop. iterator is multiplied by this value.
step = 0.5

# End value of gauges.
stop = 2.5

sizes = range(int(start), int(stop/step)+1)

# If stl output folder exists make sure it's empty, if not create it.
if not Path(stl_folder).exists():
   Path(stl_folder).mkdir(parents=True, exist_ok=True)
   print(f"Created {stl_folder}")
else:
    p = Path(stl_folder)
    for child in p.iterdir():
        Path.unlink(child)
        print(f"Removed {child}")
    print(f"Emptied {stl_folder}")

# Loop through size range run openscad command to generate stl file
for gauge in sizes:
    gauge_step = gauge * step
    if gauge_step >= start:
        print(f"Generate: {stl_folder}/radius-gauge-{gauge_step}mm.stl")
        cmd_str = "openscad -o {0}/radius-gauge-{1}mm.stl -D radius={1} radius-gauge.scad".format(stl_folder, gauge_step)
        subprocess.run(cmd_str, shell=True)
